import { Component, OnInit } from '@angular/core';
import { UtilisateursService } from "../services/utilisateurs.service";
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-utilisateurs',
  templateUrl: './utilisateurs.component.html',
  styleUrls: ['./utilisateurs.component.css']
})
export class UtilisateursComponent implements OnInit {

  public utilisateurs = []
  private sub : Subscription

  constructor(private utilisateurService : UtilisateursService) { }

  ngOnInit() {
    this.sub = this.utilisateurService.getUtilisateurs()
    .subscribe(resp => {
      console.log(resp);
      this.utilisateurs = resp
    })
  }



}
