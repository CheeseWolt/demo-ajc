from app import mysql
from flask import jsonify, render_template
from app.models.Utilisateur import Utilisateur
from app.services.UtilisateurService import UtilisateurService

class UsersController:
    def getUsers(self):
        try :
            cur = mysql.connection.cursor()
            cur.execute("SELECT u.pseudo, v.name FROM utilisateur AS u\
                INNER JOIN cities as v ON u.city_id = v.id")
            resp = cur.fetchall()
            cur.close()
            if resp is not None:
                users = []
                for user in resp:
                    users.append({
                        'pseudo': user[0],
                        'ville': user[1]
                    })
                return jsonify(users)
            return jsonify({"message":"No users"})
        except Exception as e:
            print(e)
            return jsonify({"message":"DB error"})

    def addUser(self,request):
        try :
            cur = mysql.connection.cursor()
            cur.execute("INSERT INTO utilisateur VALUES(null, %s,%s,%s,%s)",(request.json['pseudo'], request.json['email'], request.json['password'], request.json['city_id']))
            mysql.connection.commit()
            cur.close()
            return True
        except Exception as e:
            print(e)
            return False

    def listerUser(self):
        user = Utilisateur()
        resp = user.getAll()
        users = []
        for u in resp:
            users.append(UtilisateurService().toJson(u))
        return render_template("utilisateur.html", utilisateurs=users, title="Liste des utilisateurs")