import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class VilleService {

  private url = environment.api + "/api/villes/"

  constructor(private http : HttpClient) {

  }

  public getVilles(departement:any){
    let obs : Observable<any>;

    obs = this.http.get(this.url+departement);

    return obs;
  }
}