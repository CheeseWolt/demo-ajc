# Todos
## Qu'est-ce ?
C'est un site web développé sous [Flask](https://flask.palletsprojects.com/en/1.1.x/), un framework web Python.  
Les pages HTML sont interprétés par le moteur de template [Jinja](https://jinja.palletsprojects.com/en/2.11.x/).
## Comment lancer ce projet ?
### Il va vous falloir un environnement virtuel !
Dans un terminal, rendez-vous dans le repertoire du projet et tapez:
    
    py -m venv ./venv
### Activez votre environnement virtuel:

    .\venv\Scripts\activate
### Installez les dépendances
Si vous êtes sous Windows il va vous falloir installer le client mysqldb pour windows, il est fournis dans cette application:

    pip install .\mysqlclient-1.4.6-cp38-cp38-win32.whl  

Le reste des dépendences sont listées dans le fichier requirements.txt, pour les installer:  

    pip install -r requirements.txt
### Lancer le serveur Flask
Flask fait partit des dépendences installées précédemment, il suffit de taper:  

    flask run

Le site serra accessible à l'adresse [127.0.0.1:5000](http://127.0.0.1:5000/)  
Les routes pour l'api commenceront par [/api/](http://127.0.0.1:5000/api/todos)