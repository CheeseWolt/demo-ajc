import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import bcrypt from "bcryptjs";
import { environment } from 'src/environments/environment.prod';


@Injectable({
  providedIn: 'root'
})
export class UtilisateursService {
  private url = environment.api + "/api/users"

  constructor(private http: HttpClient) {

  }

  public getUtilisateurs() {
    let obs: Observable<any>;

    obs = this.http.get(this.url);

    return obs;
  }

  public addUser(pseudo: String, email: String, password: String, city_id: number) {
    password = bcrypt.hashSync(password, 10)
    return this.http.post(this.url,{
      "pseudo":pseudo,
      "email":email,
      "password":password,
      "city_id":city_id,
    },
    )
  }
}