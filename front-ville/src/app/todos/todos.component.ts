import { Component, OnInit, OnDestroy } from '@angular/core';
import { TodosService } from "../services/todos.service";
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { ReactiveFormsModule, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit, OnDestroy {

  private sub: Subscription
  public todos = []
  public todoForm: FormGroup

  constructor(private todoService: TodosService, private router: Router, private formBuilder: FormBuilder) {
    this.todoForm = new FormGroup({
      title: new FormControl("",Validators.required),
      description: new FormControl("")
    })
  }

  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe()
    }
  }

  ngOnInit() {
    this.getTodos()
  }

  public changeDone(todo) {
    this.sub = this.todoService.changeDone(todo)
      .subscribe(() => this.getTodos())
  }

  public getTodos() {
    this.sub = this.todoService.getTodos()
      .subscribe(resp => {
        this.todos = resp
      })
  }

  public deleteTodo(todo_id) {
    this.sub = this.todoService.deleteTodo(todo_id)
      .subscribe(() => this.getTodos())
  }

  public addTodo(){
    let todo = {"title":this.todoForm.value.title,"description":this.todoForm.value.description}
    this.sub = this.todoService.addTodo(todo)
    .subscribe(()=>{
      this.getTodos()
      console.log("done")
    })
  }

}
