from app import app, mysql
from flask_mysqldb import MySQL
from flask import jsonify


class DepartementsController:

    def getDepartements(self):
        try:
            cur = mysql.connection.cursor()
            cur.execute("SELECT * FROM departments")
            resp = cur.fetchall()
            cur.close()
        except Exception as e:
            print(e)
            return jsonify({"message":e,"ok":False})
        departements = []
        for departement in resp:
            departements.append(
                {
                    "id":departement[0],
                    "region_code":departement[1],
                    "code":departement[2],
                    "name":departement[3],
                    "slug":departement[4]
                }
            )
        return jsonify(departements)

    def getDepartementsFromRegion(self, region):
        try:
            cur = mysql.connection.cursor()
            cur.execute("SELECT d.id, d.code, d.name, d.slug FROM regions as r \
                INNER JOIN departments as d ON r.code = d.region_code \
                WHERE r.name like %s\
                order by d.code;", (region,))
            resp = cur.fetchall()
        except Exception as e:
            print(e)
            return jsonify({"message":"error"})
        departements = []
        for departement in resp :
            departements.append({
                "id":departement[0],
                "code":departement[1],
                "name":departement[2],
                "slug":departement[3]
            })
        return jsonify(departements)