import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormulaireComponent } from "./formulaire/formulaire.component";
import { HomeComponent } from './home/home.component';
import { UtilisateursComponent } from "./utilisateurs/utilisateurs.component";
import { TodosComponent } from "./todos/todos.component";


const routes: Routes = [
  {path:"", redirectTo: 'home', pathMatch:"full"},
  {path:"home", component:HomeComponent, pathMatch:"full"},
  {path:"formulaire", component:FormulaireComponent, pathMatch:"full"},
  {path:"utilisateurs", component:UtilisateursComponent, pathMatch:"full"},
  {path:"todos", component:TodosComponent, pathMatch:"full"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
