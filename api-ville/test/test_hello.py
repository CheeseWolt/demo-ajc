from app import app
import pytest

def test_hello():
    resp = app.test_client().get('/')
    assert resp.status_code == 200
    assert b"Flask" in resp.data