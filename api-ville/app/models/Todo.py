from app import app, mysql

class Todo:
    def __init__(self, titre = None, description = None, done = False):
        self.titre = titre
        self.description = description
        self.done = done
    
    def getAllTodos(self):
        try:
            cur = mysql.connection.cursor()
            cur.execute("SELECT * FROM todos")
            resp = cur.fetchall()
            cur.close()
            if resp is not None:
                return resp
            else:
                return False
        except Exception as e:
            print(e)
            return e

    def getTodo(self, id):
        try:
            cur = mysql.connection.cursor()
            cur.execute("SELECT * FROM todos WHERE id = %s", (id,))
            resp = cur.fetchone()
            cur.close()
            return resp
        except Exception as e:
            print(e)
            return False
    
    def createTodo(self):
        try:
            cur = mysql.connection.cursor()
            cur.execute("INSERT INTO todos VALUES(NULL,%s,%s,%s)",(self.titre, self.description, self.done))
            mysql.connection.commit()
            cur.close()
            return True
        except Exception as e:
            print(e)
            return False

    def updateTodo(self, id):
        try:
            cur = mysql.connection.cursor()
            cur.execute("UPDATE todos SET title = %s , description = %s, done = %s WHERE id = %s",(self.titre, self.description, self.done, id))
            mysql.connection.commit()
            cur.close()
            return True
        except Exception as e :
            print(e)
            return False

    def deleteTodo(self,id):
        try:
            cur = mysql.connection.cursor()
            cur.execute("DELETE FROM todos WHERE id = %s", (id,))
            mysql.connection.commit()
            cur.close()
            return True
        except Exception as e:
            print(e)
            return False