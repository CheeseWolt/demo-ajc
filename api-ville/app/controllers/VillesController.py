from app import app, mysql
from flask_mysqldb import MySQL
from flask import jsonify


class VillesController:

    def getVilles(self):
        try:
            cur = mysql.connection.cursor()
            cur.execute("SELECT * FROM cities")
            resp = cur.fetchall()
            cur.close()
        except Exception as e:
            print(e)
            return jsonify({"message":e,"ok":False})
        villes = []
        for ville in resp:
            villes.append(
                {
                    "id":ville[0],
                    "region_code":ville[1],
                    "code":ville[2],
                    "name":ville[3],
                    "slug":ville[4]
                }
            )
        return jsonify(villes)

    def getVillesFromDepartement(self, departement):
        try:
            cur = mysql.connection.cursor()
            cur.execute("SELECT cities.id, cities.zip_code, cities.name, cities.slug FROM cities \
                INNER JOIN departments ON departments.code = cities.department_code \
                WHERE departments.name like %s\
                order by cities.zip_code;", (departement,))
            resp = cur.fetchall()
            villes = []
            for ville in resp :
                villes.append({
                    "id":ville[0],
                    "zipcode":ville[1],
                    "name":ville[2],
                    "slug":ville[3]
                })
            return jsonify(villes)
        except Exception as e:
            print(e)
            return jsonify({"message":"error"})
        return jsonify({"message":"Aucune villes trouvées"})