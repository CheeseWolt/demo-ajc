import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import { environment } from "../../environments/environment.prod";

@Injectable({
  providedIn: 'root'
})
export class DepartementService {

  private url = environment.api + "/api/departements/"

  constructor(private http : HttpClient) {

  }

  public getDepartements(region : any){
    let obs : Observable<any>;

    obs = this.http.get(this.url+region);

    return obs;
  }
}
