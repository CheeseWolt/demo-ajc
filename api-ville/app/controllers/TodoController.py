from app import app, mysql
from flask import jsonify, render_template, redirect, url_for
from app.models.Todo import Todo
from app.services.TodoService import TodoService

class TodoController:

    def getTodos(self):
        todos= []
        todo = Todo()
        resp = todo.getAllTodos()
        for t in resp:
            todos.append(TodoService().toJson(t))
        return jsonify( todos)

    def getTodo(self, id):
        todos= []
        todo = Todo()
        resp = todo.getTodo(id)
        if resp:
            t = TodoService().toJson(resp)
            return jsonify(t)
        else:
            return resp

    def addTodo(self, request):
        try:
            cur = mysql.connection.cursor()
            cur.execute("INSERT INTO todos values(null,%s, %s, false)", (request.json["title"],request.json["description"]))
            mysql.connection.commit()
            cur.close()
            return jsonify({"message":"Todo ajouté"})
        except Exception as e:
            print(e)
            return jsonify({"message":"DB error"})

    def updateTodo(self, todo_id, request):
        try:
            cur = mysql.connection.cursor()
            cur.execute("UPDATE todos SET title = %s, description = %s, done = %s WHERE id = %s", (request.json["title"],request.json["description"], request.json["done"], todo_id))
            mysql.connection.commit()
            cur.close()
            return jsonify({"message":"Todo modifié"})
        except Exception as e:
            print(e)
            return jsonify({"message":"DB error"})

    def deleteTodo(self, todo_id):
        try:
            cur = mysql.connection.cursor()
            cur.execute("DELETE FROM todos WHERE id = %s", (todo_id,))
            mysql.connection.commit()
            cur.close()
            return jsonify({"message": "Todo supprimé"})
        except Exception as e:
            print(e)
            return jsonify({"message": "DB error"})
    
    def listerTodos(self):
        todos= []
        todo = Todo()
        resp = todo.getAllTodos()
        for t in resp:
            todos.append(TodoService().toJson(t))
        return render_template("todo/todo.html",todos=todos, title="Todo list")

    def modifierTodo(self, todo_id , request):
        t = Todo().getTodo(todo_id)
        todo = Todo(t[1],t[2],t[3])
        if request.form.get("titre") is not None and request.form.get("description") is not None:
            todo.titre          = request.form["titre"]
            todo.description    = request.form["description"]
            todo.done           = True if request.form.get("done") == "on" else False
            todo.updateTodo(todo_id)
            return redirect(url_for("accueilTodo"))
        return render_template("todo/modifTodo.html", titre=todo.titre, description=todo.description, done=todo.done)

    def creerTodo(self, request):
        if request.form.get("titre") is not None and request.form.get("description") is not None:
            todo = Todo(request.form["titre"],request.form["description"],True if request.form.get("done") == "on" else False)
            todo.createTodo()
            return redirect(url_for("accueilTodo"))
        return render_template("todo/newTodo.html")

    def supprimerTodo(self, id):
        Todo().deleteTodo(id)
        return redirect(url_for("accueilTodo"))