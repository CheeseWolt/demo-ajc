import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from "@angular/forms";
import { Subscription } from 'rxjs';
import { RegionService } from "../services/region.service";
import { DepartementService } from '../services/departement.service';
import { VilleService } from '../services/ville.service';
import { UtilisateursService } from "../services/utilisateurs.service";

@Component({
  selector: 'app-formulaire',
  templateUrl: './formulaire.component.html',
  styleUrls: ['./formulaire.component.css']
})
export class FormulaireComponent implements OnInit, OnDestroy {

  public inscription: FormGroup;
  private sub : Subscription[] = [];
  public regions = [];
  public departements = [];
  public villes = [];
  constructor(
    private formBuilder: FormBuilder, 
    private regionService : RegionService,
    private departementService: DepartementService,
    private villeService: VilleService,
    private utilisateurService: UtilisateursService
    ) {
    this.inscription = new FormGroup({
      pseudo: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.minLength(5)]),
      password_confirm: new FormControl('', [Validators.minLength(5)]),
      region: new FormControl('', Validators.required),
      departement: new FormControl('', Validators.required),
      ville: new FormControl('', Validators.required)
    })
  }

  ngOnInit() {
    this.fillRegions()
    console.log(this.inscription)
  }

  ngOnDestroy(){
    if(this.sub)
    this.sub.forEach(sub=> sub.unsubscribe())
  }

  public fillRegions() {
    this.sub.push(this.regionService.getRegions()
    .subscribe(resp => {
      this.regions = resp
    }))
  }

  public fillDepartements() {
    console.log(this.inscription)
    this.sub.push( this.departementService.getDepartements(this.inscription.value.region)
    .subscribe(resp => {
      this.departements = resp
    }))
  }

  public fillVilles() {
    this.sub.push( this.villeService.getVilles(this.inscription.value.departement)
    .subscribe(resp => {
      this.villes = resp
    }))
  }

  public submit(){
    this.utilisateurService.addUser(this.inscription.value.pseudo,
      this.inscription.value.email,
      this.inscription.value.password,
      this.inscription.value.ville)
      .subscribe(resp => console.log(this.inscription.value))
  }

}
