class TodoService:
    def toJson(self, todo):
        return {
            "id":todo[0],
            "titre":todo[1],
            "description":todo[2],
            "done":todo[3],
        }