import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class RegionService {

  private url = environment.api + "/api/regions"

  constructor(private http : HttpClient) {

  }

  public getRegions(){
    let obs : Observable<any>;

    obs = this.http.get(this.url);

    return obs;
  }
}
