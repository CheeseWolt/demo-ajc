from app import app, mysql
from flask_mysqldb import MySQL
from flask import jsonify
import json


class RegionsController:

    def getRegions(self):
        try:
            cur = mysql.connection.cursor()
            cur.execute("SELECT * FROM regions")
            resp = cur.fetchall()
            cur.close()
        except Exception as e:
            print(e)
            return jsonify({"message":e,"ok":False})
        regions = []
        for region in resp:
            regions.append(
                {
                    "id":region[0],
                    "code":region[1],
                    "name":region[2],
                    "slug":region[3]
                }
            )
        return jsonify(regions)
