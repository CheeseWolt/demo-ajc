from app import app
from flask import request, redirect, url_for, render_template
from app.controllers.RegionsController      import RegionsController
from app.controllers.DepartementsController import DepartementsController
from app.controllers.VillesController       import VillesController
from app.controllers.UsersController        import UsersController
from app.controllers.TodoController         import TodoController

RC = RegionsController() 
DC = DepartementsController() 
VC = VillesController()
UC = UsersController()
TC = TodoController()

@app.route('/')
def home():
    return render_template("home.html")

@app.route('/api/regions', methods=["GET"])
def getRegions():
    return RC.getRegions()

@app.route('/api/departements', methods=["GET"])
def getDepartements():
    return DC.getDepartements()

@app.route('/api/departements/<string:region>', methods=["GET"])
def getDepartementsFromRegion(region):
    return DC.getDepartementsFromRegion(region)

@app.route('/api/villes', methods=["GET"])
def getVilles():
    return VC.getVilles()

@app.route('/api/villes/<string:departement>', methods=["GET"])
def getVillesFromDepartement(departement):
    return VC.getVillesFromDepartement(departement)

@app.route("/api/users", methods=["GET"])
def getUsers():
    return UC.getUsers()

@app.route("/api/users", methods=["POST"])
def addUser():
    if UC.addUser(request):
        return redirect(url_for("/api/users")), 200
    else:
        return "Error"

@app.route("/api/todos", methods=["GET"])
def getTodos():
    return TC.getTodos()

@app.route("/api/todos/<int:id>", methods=["GET"])
def getTodo(id):
    return TC.getTodo(id)

@app.route("/api/todos", methods=["POST"])
def addTodo():
    return TC.addTodo(request)

@app.route("/api/todos/<int:todo_id>", methods=["PUT"])
def updateTodo(todo_id):
    return TC.updateTodo(todo_id,request)

@app.route("/api/todos/<int:todo_id>", methods=["DELETE"])
def deleteTodo(todo_id):
    return TC.deleteTodo(todo_id)

@app.route("/todos")
def accueilTodo():
    return TC.listerTodos()

@app.route("/updateTodo/<int:id>", methods=["GET","POST"])
def modifTodo(id):
    return TC.modifierTodo(id, request)

@app.route("/createTodo", methods=["GET", "POST"])
def creerTodo():
    return TC.creerTodo(request)

@app.route("/deleteTodo/<int:id>")
def supprimerTodo(id):
    return TC.supprimerTodo(id)

@app.route("/users")
def accueilUser():
    return UC.listerUser()
