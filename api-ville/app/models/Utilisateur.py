from app import mysql

class Utilisateur:
    def __init__(self, pseudo=None, email=None, mdp=None, city_id=None):
        self.pseudo = pseudo
        self.email = email
        self.mdp = mdp
        self.city_id = city_id

    def getAll(self):
        try:
            cur = mysql.connection.cursor()
            cur.execute("SELECT pseudo, email FROM utilisateur")
            resp = cur.fetchall()
            cur.close()
            if resp is not None:
                return resp
            return False
        except Exception as e:
            print(e)
            return e