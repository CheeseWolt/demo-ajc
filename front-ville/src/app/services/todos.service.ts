import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';

const optionRequete = {
  headers: new HttpHeaders({ 
    'Access-Control-Allow-Origin':'*',
    'Content-Type': 'application/json'
  })
}
@Injectable({
  providedIn: 'root'
})
export class TodosService {

  private url = environment.api + "/api/todos"

  constructor(private http: HttpClient) { }

  public getTodos():Observable<any>{
    let obs: Observable<any>
    obs = this.http.get(this.url)
    return obs
  }

  public changeDone(todo):Observable<any>{
    return this.http.put(this.url+"/"+todo.id, {
      "title": todo.title,
      "description": todo.description,
      "done": !todo.done,
    },optionRequete)
  }

  public deleteTodo(todo_id){
    return this.http.delete(this.url + "/" + todo_id)
  }

  public addTodo(todo){
    return this.http.post(this.url,todo)
  }
}
