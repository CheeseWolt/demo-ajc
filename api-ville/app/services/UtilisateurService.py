class UtilisateurService:
    
    def toJson(self, utilisateur):
        return {
            "pseudo": utilisateur[0],
            "email": utilisateur[1],
        }