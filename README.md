# Projet Demo

Ceci est un projet en 3 parties:

## Front en Angular

La partie front du projet est réalisé sous Angular, elle consomme une API Flask

## Back en Python avec Flask

Le back est une appli Flask FullStack, il y a une API et un site dont les pages sont générée avec Jinja 

## Base de donnée en MySQL

Base de donnée MySQL qui contient une liste d'utilisateurs, une todolist et la liste des départements, régions et villes de France.

Le script SQL est fournis afin de faire foncitonner le projet rapidement

## Le tout est contenerisé gràce à Docker

Il y a un Dockerfile pour chaques partie de l'appli et un docker-compose afin de tout lancer rapidement.

## Gitlab et le CI-CD

Un fichier .gitlab-ci.yml permet de tester et pousser les containers à chaques push sur gitlab.